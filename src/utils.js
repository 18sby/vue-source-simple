const LIFE_CYCLES = [
  'beforeCreate',
  'created',
  'mounted',
  'beforeUpdate',
  'updated'
];
let strats = {};
function mergeHook(parentVal, childVal) {
  if (childVal) {
    if (parentVal) {
      return parentVal.concat(childVal);
    } else {
      return [childVal];
    }
  } else {
    return parentVal;
  }
};
LIFE_CYCLES.forEach(hook => {
  strats[hook] = mergeHook;
})

export function mergeOptions(parent, child) {
  const options = {};
  // 如果父亲和儿子都有一个属性，这个属性不冲突
  for (let key in parent) { // 1. 先合并 parent 的 key 到 options
    mergeField(key);
  }
  for (let key in child) { // 2. 在合并 child 中的 key (parent中没有的)
    if (!parent.hasOwnProperty(key)) {
      mergeField(key);
    }
  }

  function mergeField(key) {
    if (strats[key]) {
      options[key] = strats[key](parent[key], child[key]);
    } else {
      if (isObject(parent[key]) && isObject(child[key])) {
        options[key] = {
          ...parent,
          ...child
        }
      } else { // 不管怎样，只要 parent 和 child 不同时为对象，那么就用 child 的，因为 child 优先级高
        options[key] = child[key] || parent[key];
      }
    }
  }

  return options;
}

export function isObject(obj) {
  return obj && typeof obj === 'object';
}