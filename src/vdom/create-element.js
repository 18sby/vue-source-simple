export function createTextVnode(text) {
  return vnode(undefined, undefined, undefined, undefined, text);
}

export function createElement(tag, data = {}, ...children) {
  let key = data.key;
  if (key) {
    delete data.key;
  }

  // vue 中的 key 不会作为属性传递给组件
  return vnode(tag, data, key, children);
}

// 虚拟节点是产生一个对象，用来描述 dom 结构的
// ast 他是描述 dom 语法的
function vnode(tag, data, key, children, text) {
  return {
    tag,
    data,
    key,
    children,
    text
  }
}