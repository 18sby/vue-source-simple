import { createElement } from "./create-element";

export function patch(oldVnode, newVnode) {
  const isRealElement = oldVnode.nodeType;
  if (isRealElement) { // 真实元素
    const oldElm = oldVnode;
    const parentElm = oldElm.parentNode;
    let el = createElm(newVnode); // 创建新的节点
    parentElm.insertBefore(el, oldElm.nextSibling);
    parentElm.removeChild(oldElm);
    return el; // 渲染的真实 dom
  } else {
    // dom diff 算法 同层比较 （默认想完整比对一棵树 O(n^3)） O(n)
    // 不需要跨级比较
    
    // 两棵树，要先比较树根是否一样，再取比较儿子
    if (oldVnode.tag !== newVnode.tag) { // 标签名不一致，说明是两个不一样的节点
      oldVnode.el.parentNode.replaceChild(createElm(newVnode), oldVnode.el);
    }
    // 标签一致 都是文本，tag === undefined
    if (!oldVnode.tag ) { // 如果是文本，文本变化了，直接用新的文本替换老的文本
      if (oldVnode.text !== newVnode.text) {
        oldVnode.textContent = newVnode.text;
      }
    }
    // 一定是标签了 而且标签一致
    // 需要复用老的节点，替换掉老的属性
    let el = newVnode.el = oldVnode.el;
    // 更新属性 diff 属性
    updateProperties(newVnode, oldVnode.data);
    
    // 比对孩子节点
    let oldChildren = oldVnode.children || []; // 老的孩子
    let newChildren = newVnode.children || []; // 新的孩子
    
    // 新老都有孩子，那就比较，diff 核心
    // 新的有孩子，老的没孩子，直接插入新的孩子
    // 新的没孩子，老的有孩子，直接删除老的孩子
    if (oldChildren.length > 0 && newChildren.length > 0) {
      // diff 操作的核心，两个人都有儿子
      updateChildren(el, oldChildren, newChildren);

    } else if (oldChildren.length > 0) {
      el.innerHTML = '';
    } else if (newChildren.length > 0) {
      for (let i = 0; i < newChildren.length; i++) {
        let child = newChildren[i];
        el.appendChild(createElm(child));
      }
    }
    

    return el;
  }
}

function isSameVnode(oldVnode, newVnode) {
  return (oldVnode.key === newVnode.key) && (oldVnode.tag === newVnode.tag);
}

function makeIndexByKey(children) {
  let map = {};
  children.forEach((item, index) => {
    map[item.key] = index;
  });
  return map;
}

// diff 核心 比对新老儿子
function updateChildren(parent, oldChildren, newChildren) {
  // vue 2.0 使用双指针的方式来进行比对
  // v-for 要有 key
  let oldStartIndex = 0; // 老的开始
  let oldStartVnode = oldChildren[0]; // 老的开始的索引
  let oldEndIndex = oldChildren.length - 1; // 老的结束索引
  let oldEndVnode = oldChildren[oldEndIndex]; // 老的孩子的最后一个

  let newStartIndex = 0; // 新的开始
  let newStartVnode = newChildren[0]; // 新的开始的索引
  let newEndIndex = newChildren.length - 1; // 新的结束索引
  let newEndVnode = newChildren[newEndIndex]; // 新的孩子的最后一个
  
  let map = makeIndexByKey(oldChildren);
  // 比较时，采用孩子数最少的进行比较，剩下的要不是删除，要不是增加
  while (oldStartIndex <= oldEndIndex && newStartIndex <= newEndIndex) {
    if (!oldStartVnode) {
      oldStartVnode = oldChildren[++oldStartIndex];
    } else if (!oldEndVnode) {
      oldEndVnode = oldChildren[--oldEndIndex];
    } else if (isSameVnode(oldStartVnode, newStartVnode)) {
      // 如何判断两个虚拟节点是否一致，就是用 key + tag 进行判断
      // 方案1，先开始从头部进行比较 O(n)  优化用户向后插入
      // 标签和 key 一致，元素属性可能不一致
      patch(oldStartVnode, newStartVnode); // 自身属性 + 递归比较
      oldStartVnode = oldChildren[++oldStartIndex];
      newStartVnode = newChildren[++newStartIndex];
    } else if (isSameVnode(oldEndVnode, newEndVnode)) {
      // 方案2 从尾部开始比较，如果头部不一致，开始尾部比较 优化用户向前插入
      patch(oldEndVnode, newEndVnode);
      oldEndVnode = oldChildren[--oldEndIndex];
      newEndVnode = newChildren[--newEndIndex];
    } else if (isSameVnode(oldStartVnode, newEndVnode)) { // 正序和倒序 sort reverse
      // 方案3 头不一样 尾不一样 比较头和尾
      patch(oldStartVnode, newEndVnode);
      parent.insertBefore(oldStartVnode.el, oldEndVnode.el.nextSibling);
      oldStartVnode = oldChildren[++oldStartIndex];
      newEndVnode = newChildren[--newEndIndex];
    } else if (isSameVnode(oldEndVnode, newStartVnode)) {
      // 方案4 头不一样 尾不一样 比较尾和头
      patch(oldEndVnode, newStartVnode);
      parent.insertBefore(oldEndVnode.el, oldStartVnode.el);
      oldEndVnode = oldChildren[--oldEndIndex];
      newStartVnode = newChildren[++newStartIndex];
    } else {
      // 方案5 乱序比对
      let moveIndex = map[newStartVnode.key];
      if (moveIndex == undefined) {
        parent.insertBefore(createElm(newStartVnode), oldStartVnode.el);
      } else {
        let moveVnode = oldChildren[moveIndex];
        oldChildren[moveIndex] = null;
        patch(moveVnode, newStartVnode);
        parent.insertBefore(moveVnode.el, oldStartVnode.el);
      }
      newStartVnode = newChildren[++newStartIndex];
    }
  }
  
  if (newStartIndex <= newEndIndex) {
    for (let i = newStartIndex; i <= newEndIndex; i++) {
      // appendChild === insertBefore null
      let ele = newChildren[newEndIndex+1] === null ? null : newChildren[newEndIndex+1].el;
      parent.insertBefore(createElm(newChildren[i]), ele);
      // parent.appendChild(createElm(newChildren[i]));
    }
  }
  if (oldStartIndex <= oldEndIndex) { // 说明新的已经循环完毕，老的有剩余，删掉
    for (let i = oldStartIndex; i <= oldEndIndex; i++) {
      let child = oldChildren[i];
      if (child != null) {
        parent.removeChild(child.el);
      }
    }
  }
}

function updateProperties(vnode, oldProps = {}) {
  // 需要比较 vnode 上的 data 和 oldProps 的差异
  const el = vnode.el;
  let newProps = vnode.data || {};
  // 获取老的样式和新的样式的差异 如果新的上面丢失了属性，应该在老的元素上删除

  let newStyle = newProps.style || {};
  let oldStyle = oldProps.style || {};
  for (let key in oldStyle) { // Obejct.keys
    if (!newStyle[key]) {
      el.style[key] = ''; // 删除之前的样式
    }
  }
  for (let key in oldProps) {
    if (!newProps[key]) {
      el.removeAttribute(key);
    }
  }
  // 其他情况，直接用新的值覆盖掉老的值即可
  for (let key in newProps) {
    if (key === 'style') {
      for (let styleName in newProps.style) {
        console.log('styleName: ', styleName);
        el.style[styleName] = newProps.style[styleName];
      }
    } else {
      el.setAttribute(key, newProps[key]);
    }
  }
}

export function createElm(vnode) { // 需要递归创建
  let { tag, children, data, key, text } = vnode;
  if (typeof tag === 'string') { // 元素
    // 将虚拟节点和真实节点做一个映射关系，（后面diff时，如果元素相同直接复用老元素）
    vnode.el = document.createElement(tag);
    updateProperties(vnode); // 更新元素的属性
    children.forEach(child => {
      vnode.el.appendChild( createElm(child) ); // 递归渲染子节点，将子节点渲染到父节点中
    });
  } else { // 普通文本
    vnode.el = document.createTextNode(text);
  }
  return vnode.el;
}