import { initState } from './state.js';
import { compileToFunctions } from './compiler/index.js'
import { mountComponent, callHook } from './lifecycle.js';
import { mergeOptions } from './utils.js';
import { nextTick } from './observer/scheduler.js';

export function initMixin(Vue) {
  Vue.prototype.$nextTick = nextTick;
  Vue.prototype._init = function(options) {
    // Vue 的内部 $options: 用户传递的所有参数
    const vm = this;
    vm.$options = mergeOptions(vm.constructor.options, options); // 用户传入的参数

    callHook(vm, 'beforeCreate');
    // options.data props computed watch
    initState(vm); // 初始化状态

    callHook(vm, 'created');
    // 需要通过模板进行渲染
    if (vm.$options.el) { // 用户传入了 el
      vm.$mount(vm.$options.el);
    }
  }
  Vue.prototype.$mount = function(el) { // 可能是字符串，也可以传入一个 dom 对象
    const vm = this;
    el = vm.$el = document.querySelector(el); // 获取 el 的 dom
    // 如果同时传入 template 和 render，那么会优先使用 render
    // 如果 template render 都没传，那么使用 id="app" 的模板
    const opts = vm.$options;
    if (!opts.render) {
      let template = opts.template;
      if (!template && el) { // 应该使用外部的模板
        template = el.outerHTML;
      }
      const render = compileToFunctions(template);
      opts.render = render;
    }
    // 走到这里说明不需要编译了，因为用户传入的就是 render 函数

    mountComponent(vm, el); // 组件的挂载流程
  }
}