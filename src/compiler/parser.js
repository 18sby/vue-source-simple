const ncname = `[a-zA-Z_][\\-\\.0-9_a-zA-Z]*`; // 标签名
const qnameCapture = `((?:${ncname}\\:)?${ncname})`; // ?: 匹配但是不捕获
const startTagOpen = new RegExp(`^<${qnameCapture}`); // 标签开头的正则 捕获的内容是标签名
const endTag = new RegExp(`^<\\/${qnameCapture}[^>]*>`); // 匹配标签结尾的 </div>
const attribute = /^\s*([^\s"'<>\/=]+)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/; // 匹配属性名和值
const startTagClose = /^\s*(\/?)>/; // 匹配标签结束的 >

export function parseHTML(html) {
  // ast 树 表示 html 的语法
  /*
    {
      tag: 'div',
      type: 1,
      children: [],
      attrs: [
        { name: 'id', value: 'app' }
      ],
      parent: null
    }
  */
  let root; // 树根
  let currentParent;
  let stack = []; // 用来判断标签是否正常闭合

  function createASTElement(tagName, attrs) {
    return {
      tag: tagName,
      attrs,
      children: [],
      parent: null,
      type: 1 // 1 普通元素 3 文本
    }
  }

  function start(tagName, attrs) { // 开始标签 每次解析开始标签，都会执行此方法，只有第一次是根
    let element = createASTElement(tagName, attrs);
    if (!root) {
      root = element;
    }
    currentParent = element;
    stack.push( element );
  }
  function end(tagName) { // 结束标签 确立父子关系
    let element = stack.pop();
    let parent = stack[stack.length - 1];
    if (parent) {
      element.parent = parent;
      currentParent = parent;
      parent.children.push( element );
    }
  }
  function chars(text) { // 文本
    text = text.replace(/\s/g, '');
    if (text) {
      currentParent.children.push({
        type: 3,
        text
      })
    }
  }

  // 根据 html 解析成树结构
  // <div id="app" style="color: red"><span>helloworld {{msg}}</span></div>
  while (html) {
    let textEnd = html.indexOf('<');
    if (textEnd === 0) {
      const startTagMatch = parseStartTag();
      if (startTagMatch) {
        start(startTagMatch.tagName, startTagMatch.attrs);
        // 开始标签
      }
      const endTagMatch = html.match(endTag);
      if (endTagMatch) {
        advance(endTagMatch[0].length);
        end(endTagMatch[1]);
      }
    }
    // 如果不是 0，说明是文本
    let text;
    if (textEnd > 0) {
      text = html.substring(0, textEnd); // 是文本就把文本内容截取掉
      chars(text);
    }
    if (text) {
      advance(text.length); // 删除文本内容
    }
  }

  function advance(n) {
    html = html.substring(n);
  }

  function parseStartTag() {
    const start = html.match(startTagOpen); // 匹配开始标签
    if (start) {
      const match = {
        tagName: start[1], // 匹配到的标签名
        attrs: [],
      }
      advance(start[0].length);
      let end, attr;
      while (!(end = html.match(startTagClose)) && (attr = html.match(attribute))) {
        match.attrs.push({
          name: attr[1],
          value: attr[3] || attr[4] || attr[5]
        })
        advance(attr[0].length);
      }
      if (end) {
        advance(end[0].length);
        return match;
      }
    }
  }

  return root;
}