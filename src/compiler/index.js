import { parseHTML } from './parser.js';
import { generate } from './generator';

export function compileToFunctions(template) {
  // 实现模板的编译
  let ast = parseHTML(template);
  
  // 代码生成
  // template => render 函数
  /*
    react jsx
    render() {
      with(this._data) {
        return _c('div', {id: 'app', 'style': 'color:red'}, _c('span', undefined, _v('hello world' + _s(msg))))
      }
    }
  */
  // 核心就是字符串拼接
  let code = generate(ast);
  
  code = `with(this) {
    return ${code}
  }`;
  let render = new Function(code); // 相当于给字符串变成了函数
  return render;

  // 模板编译原理
  // 1. 先把我们的代码转换为 ast 语法树 (1) parser 解析(正则)
  // 2. 标记静态树 <span>123</span> (2) 树的遍历标记 markup
  // 3. 通过 ast 产生的语法树生成代码 => render 函数 (3) codegen
}