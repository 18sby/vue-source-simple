const defaultTagRE = /\{\{((?:.|\r?\n)+?)\}\}/g;

function genProps(attrs) { // { id: 'app', style: {color: red} }
  let str = '';
  for (let i = 0; i < attrs.length; i++) {
    let attr = attrs[i]; // 每一个属性
    if (attr.name === 'style') {
      let obj = {}; // color:red;background:green
      attr.value.split(';').forEach(item => {
        let [key, value] = item.split(':');
        obj[key] = value;
      });
      attr.value = obj;
    }
    str += `${attr.name}:${JSON.stringify(attr.value)},`;
  }
  return `{${str.slice(0, -1)}}`;
}

function gen(node) {
  if (node.type === 1) {
    return generate(node);
  } else {
    let text = node.text;
    if (!defaultTagRE.test(text)) {
      return `_v(${JSON.stringify(text)})`;
    } else {
      let tokens = []; // 全局匹配的正则如果匹配过一次后，lastIndex 就变了
      let lastIndex = defaultTagRE.lastIndex = 0;
      let match, index;
      while (match = defaultTagRE.exec(text)) {
        index = match.index;
        // 通过 lastIndex  index
        tokens.push( JSON.stringify( text.slice(lastIndex, index) ) );
        tokens.push(`_s(${match[1].trim()})`);
        lastIndex = index + match[0].length;
      }
      if (lastIndex < text.length) {
        tokens.push( JSON.stringify( text.slice(lastIndex, text.length) ) );
      }
      return `_v(${tokens.join('+')})`;
    }
  }
}

function genChildren(el) {
  const children = el.children;
  if (children) {
    return children.map(c => gen(c)).join(',');
  }
  return false;
}

// 语法级别的编译
export function generate(el) {
  let children = genChildren(el); // 生成孩子字符串
  let code = `_c("${el.tag}", ${
      el.attrs.length ? `${genProps(el.attrs)}` : undefined
    }${
      el.children ? `,${children}` : ''
    })`;
  return code;
}