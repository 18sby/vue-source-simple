let has = {}; // Vue 源码里有时候用 set，有时候用对象
let queue = [];

function flushSchedulerQueue() {
  queue.forEach(watcher => {
    watcher.run();
  });
  queue.length = 0;
  has = {};
}

export function queueWatcher(watcher) {
  const id = watcher.id;
  if (has[id] === undefined) {
    has[id] = true;
    queue.push(watcher);
    nextTick(flushSchedulerQueue, 0); // 调用渲染 watcher
  }
}

let callbacks = [];
let pending = false;
function flushCallbacksQueue() {
  callbacks.forEach(callback => callback());
  pending = false
}
export function nextTick(fn) {
  callbacks.push(fn);
  if (!pending) {
    setTimeout(() => {
      flushCallbacksQueue();
    }, 0);
    pending = true;
  }
}