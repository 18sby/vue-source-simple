let id = 0;

export class Dep {
  constructor() {
    this.id = id++;
    this.subs = [];
  }
  depend() {
    // this.watchers.push(Dep.target);
    Dep.target.addDep(this); // 让 watcher 存储 dep
  }
  addSub(watcher) {
    this.subs.push(watcher);
  }
  notify() {
    this.subs.forEach(watcher => watcher.update());
  }
}

Dep.target = null;
const stack = []; // 计算属性

export function pushTarget(watcher) {
  Dep.target = watcher;
  // stack.push(watcher);
}

export function popTarget(watcher) {
  Dep.target = null;
  // stack.pop();
  // Dep.target = stack[stack.length - 1];
}

// 每个属性 都有一个 dep 属性，dep 存放着 多个 watcher，一个 watcher 被多个 dep 依赖
// dep 和 watcher 是多对多的关系