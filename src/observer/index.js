import { isObject } from '../utils.js'
import { arrayMethods } from './array.js';
import { Dep } from './dep.js';

class Observer {
  constructor(data) {
    // 在数据上获取 __ob__ 属性，指的是 Observer 的实例
    Object.defineProperty(data, '__ob__', {
      enumerable: false,
      configurable: false,
      value: this
    })
    // 对数组索引进行拦截，性能差，而且直接更改数组索引的方式并不多
    if (Array.isArray(data)) {
      // Vue 如何对数组进行处理呢？重写数组的方法 函数劫持 AOP
      // 改变数组本身的方法就可以监控到了
      data.__proto__ = arrayMethods; // 通过原型链向上查找的方式
      this.observeArray(data); // 对数组的每一项都进行了一次监测
    } else {
      this.walk(data); // 可以对数据一步一步的处理
    }
  }
  observeArray(data) {
    for (let i = 0, j = data.length; i < j; i++) {
      observe(data[i]);
    }
  }
  walk(data) {
    // 对象的循环 data: { msg: 'zf', age: 11 }
    Object.keys(data).forEach(key => {
      defineReactive(data, key, data[key]); // 定义响应式的数据变化
    })
  }
}

function defineReactive(data, key, value) {
  let dep = new Dep();
  observe(value); // 如果传入的值还是对象的话，就递归循环监测
  Object.defineProperty(data, key, {
    get() {
      Dep.target && dep.depend(); // 让 dep 去收集 watcher
      return value;
    },
    set(newValue) {
      if (newValue === value) return ;
      observe(newValue); // 监控当前设置的值，有可能用户设置了新值
      value = newValue;
      // 更新数据的时候，通知 dep 依赖的 watcher 去更新
      dep.notify();
    }
  })
}

export function observe(data) {
  // 对象就是使用 defineProperty 实现响应式的原理
  // 如果这个数据不是对象，或者是 null，就不用监控了
  if (!isObject(data)) {
    return ;
  }
  // 对数据进行 defineProperty
  return new Observer(data); // 可以看到当前的数据是否被观测过
}

