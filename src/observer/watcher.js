import { pushTarget, popTarget } from './dep.js';
import { queueWatcher } from './scheduler.js';

let id = 0; // 做一个 watcher 的id，每次创建 watcher 时，都有一个序号
// 目前只有一个 watcher，渲染 watcher，只要视图使用了这个属性，属性变化就要更新视图

export default class Watcher {
  constructor(vm, exprOrFn, cb, options) {
    this.vm = vm;
    this.exprOrFn = exprOrFn;
    this.cb = cb;
    this.options = options;
    this.deps = [];
    this.depsId = new Set();
    if (typeof exprOrFn === 'function') {
      this.getter = exprOrFn;
    }
    this.id = id++;
    this.get();
  }
  get() {
    pushTarget(this); // 在取值之前将 watcher 先保存起来
    this.getter(); // 这句话就实现了视图的渲染 -> 操作是取值
    popTarget(this); // 将 watcher 删掉
  }
  addDep(dep) {
    let id = dep.id;
    if (!this.depsId.has(id)) {
      this.depsId.add(id);
      this.deps.push(dep);
      dep.addSub(this); // 让当前 dep 订阅这个 watcher
    }
  }
  update() { // 更新原理
    queueWatcher(this); // 将 watcher 存储起来
    // this.get(); // 以前调用 get 方法是直接更新视图
  }
  run() {
    this.get();
  }
}