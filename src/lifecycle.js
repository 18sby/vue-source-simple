import Watcher from './observer/watcher.js';
import { patch } from './vdom/patch.js';

export function lifecycleMixin(Vue) {
  Vue.prototype._update = function(vnode) {
    const vm = this;
    // 将虚拟节点变成真实节点替换掉 $el
    // 后续 dom diff 也会执行此方法
    vm.$el = patch(vm.$el, vnode);
  }
}

export function mountComponent(vm, el) {
  // vue 在渲染的过程中会创建一个 渲染 watcher，只用来渲染
  // watcher 就是一个回调，每次数据变化，就会重新执行 watcher
  
  callHook(vm, 'beforeMount');
  const updateComponent = () => {
    // 内部会调用刚才解析后的 render 方法 => vnode
    // _render => vm.options.render 方法
    // _update => 将虚拟dom 变成真实dom
    console.log( '--------' );
    vm._update(vm._render());
  }

  // 每次数据变化就执行 updateComponent 方法进行更新操作
  new Watcher(vm, updateComponent, () => {}, true);

  callHook(vm, 'mounted');
  // vue 响应式数据的规则，数据变了，视图会刷新
}

export function callHook(vm, hook) { // vm.$options
  let handlers = vm.$options[hook];
  if (handlers) {
    for (let i = 0; i < handlers.length; i++) {
      handlers[i].call(vm); // 所有的生命周期的 this 指向的都是当前实例
    }
  }
}