import babel from 'rollup-plugin-babel';
import serve from 'rollup-plugin-serve';

export default {
  input: './src/index.js',
  output: {
    format: 'umd', // amd commonjs 默认将打包后的结果挂载到 window 上
    file: 'dist/vue.js', // 打包出的 js 文件
    name: 'Vue',
    sourcemap: true
  },
  plugins: [
    babel({
      exclude: 'node_modules/**', // 排除文件的操作 glob
    }),
    serve({
      open: true,
      openPage: '/public/index.html',
      port: 3000,
      contentBase: './'
    })
  ]
}